import React from 'react';
import { Navigation } from './components/navigation/Navigation';
import './App.css';
import { hot } from "react-hot-loader";

function App() {
  return (
    <div className="App">
      <Navigation />
    </div>
  );
}

export default hot(module)(App);
