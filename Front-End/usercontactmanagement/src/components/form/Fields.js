import React, { useRef } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import SimpleReactValidator from 'simple-react-validator';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  buttonAlignment: {
    textAlign: 'right'
  },
  validatorText: {
    color: 'red'
  }
}));

export const Fields = (props) => {
  const { readOnly = false } = props;
  const validator = useRef(new SimpleReactValidator());
  const classes = useStyles();
  
  return(
    <FormControl className={classes.formControl} >
      <TextField {...props} variant="outlined"
       size="small"
       onBlur={props.validation ? validator.current.showMessageFor(props.name) : null}
       InputProps={{readOnly: readOnly}}
      />
      {props.validation ? <div className={classes.validatorText}>{validator.current.message(props.name, props.value, props.validation)}</div> : null}
    </FormControl>
  );
}
