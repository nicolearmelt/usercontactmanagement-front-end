import React, { useState, useEffect } from 'react';
import { DataGrid, GridToolbar } from '@material-ui/data-grid';
import { UserModal } from '../modal/UserModal';
import { userServices } from '../../services/userservices';

const columns = [
  { field: 'id', headerName: 'UserId', width: 100},
  { field: 'fullName', headerName: 'Name', width: 200},
  { field: 'email', headerName: 'Email', width: 200},
  { field: 'mobileNumber', headerName: 'Mobile Number', width: 150},
  { field: 'gender', headerName: 'Gender', width: 100},
  { field: 'billingAddress', headerName: 'Billing Address', width: 200},
  { field: 'deliveryAddress', headerName: 'Delivery Address', width: 200}
];


export const DataTable = () => {

  const getUserData = async () => {
    setLoading(true);
    //for demo purposes to show the loader that's why it is wrapped in setTimout
    setTimeout(() =>userServices.getData()
      .then(response => {
        setDataRow(response.data)
        setLoading(false)
      }), 2000
    )
  }

  const rowClick = (item, index) => {
    if(item.field === 'fullName') {
      setOpen(true);
      setDetails(item.row);
    }
    
  };

  const successUpdate = (status) => {
    setDataUpdated(true);
  }

  const closeModal = () => {
    setOpen(false);
  };
  const [details, setDetails] = useState();
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [dataRow, setDataRow] = useState([]);
  const [dataUpdated, setDataUpdated] = useState(false);

  useEffect(() => {
    getUserData();
    if(dataUpdated) {
      getUserData().then(() => setDataUpdated(false));
    }
},[dataUpdated]);
  return (
    <div style={{ height: 500, width: '100%' }}>
      <DataGrid rows={dataRow} 
        columns={columns} 
        loading={loading} 
        rowsPerPageOptions={[5, 10, 20]} 
        pageSize={5} 
        onCellClick={(item, index) => rowClick(item, index)} 
        components={{Toolbar: GridToolbar,}}/>
        <UserModal isOpen={open} closeModal={() => closeModal()} userInfo={details} dataUpdate={successUpdate} />
    </div>
  );
}