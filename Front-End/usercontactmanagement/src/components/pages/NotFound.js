import React from 'react';

export const NotFound = () => {
  return(
    <p>404 Page Not Found</p>
  );
}