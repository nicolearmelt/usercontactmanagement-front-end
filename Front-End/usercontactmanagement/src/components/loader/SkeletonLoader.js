import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

export const SkeletonLoader = (props) => {
  return (<Skeleton {...props}/>)
}