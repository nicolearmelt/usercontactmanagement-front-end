import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

export const ViewDetails = (info) => {
  const classes = useStyles();
  
  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
    >
      <ListItem button>
        <ListItemAvatar>
          <Avatar>{info.userInfo?.fullName.charAt(0)}</Avatar>
        </ListItemAvatar>
        <ListItemText>{info.userInfo?.fullName}</ListItemText>
      </ListItem>
      <ListItem button>
        <ListItemText>{info.userInfo?.email}</ListItemText>
      </ListItem>
      <ListItem button>
        <ListItemText>{info.userInfo?.mobileNumber}</ListItemText>
      </ListItem>
      <ListItem button>
        <ListItemText>{info.userInfo?.billingAddress}</ListItemText>
      </ListItem>
      <ListItem button>
        <ListItemText>{info.userInfo?.deliveryAddress}</ListItemText>
      </ListItem>
    </List>
  );
}