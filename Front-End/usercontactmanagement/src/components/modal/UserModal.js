import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ViewDetails } from './ViewDetails';
import { EditDetails } from './EditDetails';

export const UserModal = ({isOpen, closeModal, userInfo, dataUpdate}) => {
  const [editMode, setEditMode] = useState(false);
  const closeEditModal = () => {
    setEditMode(false);
    closeModal();
  }
  return (
    <div>
      <Dialog open={isOpen} onClose={closeEditModal} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">User Contact Detail
        </DialogTitle>
        <DialogContent>
          {
            !editMode ? <ViewDetails userInfo={userInfo} /> : <EditDetails userId={userInfo.id} backButton={setEditMode} dataUpdate={dataUpdate}/>
          }
          
        </DialogContent>
        <DialogActions>
          { editMode ? null :
          <div>
          <Button onClick={closeModal} color="primary">
            Cancel
          </Button>

          <Button onClick={() => setEditMode(true)} color="primary" hidden={editMode}>
              Edit
            </Button>
            </div>
        }
        </DialogActions>
      </Dialog>
    </div>
  );
}