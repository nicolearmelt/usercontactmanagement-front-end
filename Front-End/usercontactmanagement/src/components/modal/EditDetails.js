import React, { useState, useEffect, useRef } from 'react'
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types'; 
import { userServices } from '../../services/userservices';
import SimpleReactValidator from 'simple-react-validator';
import Snackbar from '@material-ui/core/Snackbar';
import { Fields } from '../form/Fields';
import { SkeletonLoader } from '../loader/SkeletonLoader';
import { DataStatus } from '../dataStatus/DataStatus';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: '500px',
    minHeight: '500px'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  buttonAlignment: {
    textAlign: 'right'
  },
  centerAlign: {
    textAlign: 'right'
  }
}));

export const EditDetails = ({userId, closeModal, backButton, dataUpdate}) => {
  const [userValues, setUserValues] = useState({
    userId: '',
    firstName: '',
    lastName: '',
    email: '',
    mobileNumber: '',
    gender: '',
    address: [
      {
      addressId: '',
      address: '',
      city: '',
      postalCode: '',
      notes: '',
      addressType: '',
      isDefault: ''
    },
    {
      addressId: '',
      address: '',
      city: '',
      postalCode: '',
      notes: '',
      addressType: '',
      isDefault: ''
    }
    ]
  });
  const [snackBar, setSnackBar] = React.useState({
    open: false,
    vertical: 'top',
    horizontal: 'center',
    message: '',
    severity: ''
  });
  const validator = useRef(new SimpleReactValidator());
  const [isLoading, setLoading] = useState(false);
  const classes = useStyles();

  const userTextFields = [
    {
      id: 'userId',
      name: 'userId',
      label: 'UserId',
      value: userValues.userId,
      readOnly: true
    },
    {
      id: 'firstName',
      name: 'firstName',
      label: 'First Name',
      value: userValues.firstName,
      onChange: (event) => handleChange(event,'firstName'),
      validation: 'required|min:2|alpha_space'
    },
    {
      id: 'lastName',
      name: 'lastName',
      label: 'Last Name',
      value: userValues.lastName,
      onChange: (event) => handleChange(event,'lastName'),
      validation: 'required|min:2|alpha_space'
    },
    {
      id: 'email',
      name: 'email',
      label: 'Email',
      value: userValues.email,
      onChange: (event) => handleChange(event,'email'),
      validation: 'required|email'
    },
    {
      id: 'mobileNumber',
      name: 'mobileNumber',
      label: 'Mobile Number',
      value: userValues.mobileNumber,
      onChange: (event) => handleChange(event,'mobileNumber'),
      validation: 'required|phone'
    }
  ];

  const billingAddressFields = [
    {
      id: 'billingAddress',
      name: 'address',
      label: 'Street Address',
      value: userValues.address[0].address || '',
      onChange: (event) => handleChangeArray(event, 'address', 1, 'address'),
      validation: 'required|min:10|alpha_num_dash_space'
    },
    {
      id: 'billingCity',
      name: 'city',
      label: 'City',
      value: userValues.address[1].city || '',
      onChange: (event) => handleChangeArray(event, 'address', 1, 'city'),
      validation: 'required'
    },
    {
      id: 'billingPostalCode',
      name: 'postalCode',
      label: 'Postal Code',
      value: userValues.address[1].postalCode || '',
      onChange: (event) => handleChangeArray(event, 'address', 1, 'postalCode'),
      validation: 'required|alpha_num_dash_space'
    },
    {
      id: 'billingNotes',
      name: 'notes',
      label: 'Notes',
      value: userValues.address[1].notes || '',
      onChange: (event) => handleChangeArray(event, 'address', 1, 'notes')
    }
  ]

  const deliveryAddressFields = [
    {
      id: 'deliveryAddress',
      name: 'address',
      label: 'Street Address',
      value: userValues.address[0].address || '',
      onChange: (event) => handleChangeArray(event, 'address', 0, 'address'),
      validation: 'required|min:10|alpha_num_dash_space'
    },
    {
      id: 'deliveryCity',
      name: 'city',
      label: 'City',
      value: userValues.address[0].city || '',
      onChange: (event) => handleChangeArray(event, 'address', 0, 'city'),
      validation: 'required'
    },
    {
      id: 'deliveryPostalCode',
      name: 'postalCode',
      label: 'Postal Code',
      value: userValues.address[0].postalCode || '',
      onChange: (event) => handleChangeArray(event, 'address', 0, 'postalCode'),
      validation: 'required|alpha_num_dash_space'
    },
    {
      id: 'deliveryNotes',
      name: 'notes',
      label: 'Notes',
      value: userValues.address[0].notes || '',
      onChange: (event) => handleChangeArray(event, 'address', 0, 'notes')
    }
  ]

  const skeletonLoader = [
    {
      variant: 'text',
      animation: 'wave'
    },
    {
      variant: 'text',
      animation: 'wave'
    },
    {
      variant: 'text',
      animation: 'wave'
    },
    {
      variant: 'rect',
      animation: 'wave',
      height: 60
    },
  ];

  const handleCloseSb = () => {
    setSnackBar({ ...snackBar, open: false, message: '', severity: '' });
  };

  const getAllDetails = (userId) => {
    setLoading(true);
    userServices.getDataById(userId)
      .then(response => {
        setTimeout(() => { 
          setUserValues(response.data);
          setLoading(false);
        }, 2000 )
    });
  };

  const setDataUpdated = () => {
    console.log('UPDATE');
    dataUpdate(true);
  }

  const updateDetails = async (userId, data) => {
    setLoading(true);
    userServices.updateData(userId, data)
      .then(() => { 
        setLoading(false);
        setSnackBar((prevState) =>({...prevState, open: true, message: `Successfully edited UserId: ${userId}`, severity: 'success'}));
        setDataUpdated();
      })
      .catch(
        () => {
        setLoading(false);
        setSnackBar((prevState) =>({...prevState, open: true, message: `Something went wrong editing UserId: ${userId}`, severity: 'error'}));
      });
  }

  const deleteData = (userId) => {
    setLoading(true);
    userServices.deleteData(userId)
      .then(() => { 
        setLoading(false);
        setSnackBar((prevState) =>({...prevState, open: true, message: `Successfully deleted UserId: ${userId}`, severity: 'success'}));
        dataUpdate(true);
      })
      .catch(
        () => {
        setLoading(false);
        setSnackBar((prevState) =>({...prevState, open: true, message: `Something went wrong deleting UserId: ${userId}`, severity: 'error'}));
      });;
    closeModal();
  }

  const submitData = (event) => {
    event.preventDefault();
    if(validator.current.allValid()) {
      updateDetails(userId, userValues);
    } else {
      validator.current.showMessages();
    }
    
  }

  const handleChange = (event, field) => {
    setUserValues(prevState => ({...prevState, [field]: event.target.value}));
  }

  const handleChangeArray = (event, parent, index, field) => {
    setUserValues((prevState) => {prevState[parent][index][field] = event.target.value; return({...prevState})});
  }

  useEffect(() => {
    getAllDetails(userId)}
  ,[]);

  const { vertical, horizontal, open, message, severity } = snackBar;

  return(
    <>
    <div className={classes.root} onBlur={closeModal}>
      <form onSubmit={submitData}>
        <fieldset>
          <legend>User Details</legend>
          {isLoading ? <div>{skeletonLoader.map((item, index) => <SkeletonLoader {...item} key={index}/>)}</div>
          : userTextFields.map((item, index) => <Fields {...item} key={index} validator={validator}/>)}
        </fieldset>
        <fieldset>
        <legend>Billing Address</legend>   
          {isLoading ? <div>{skeletonLoader.map((item, index) => <SkeletonLoader {...item} key={index}/>)}</div>: billingAddressFields.map((item, index) => <Fields {...item} key={index} validator={validator}/>)}                                                                                                          
        </fieldset>
        <fieldset>
          <legend>Delivery Address</legend>
          {isLoading ? <div>{skeletonLoader.map((item, index) => <SkeletonLoader {...item} key={index}/>)}</div>: deliveryAddressFields.map((item, index) => <Fields {...item} key={index} validator={validator}/>)}    
        </fieldset>
        <div className={classes.buttonAlignment}>
        <Button type="button" color="secondary" onClick={() => deleteData(userValues.userId)}>Delete</Button>
        <Button type="button" color="primary" onClick={() => backButton(false)}>Back</Button>
        <Button type="submit" color="primary">Save</Button>
        </div>
      </form>
      <DataStatus
        anchorOrigin={{ vertical, horizontal }}
        open={open}
        message={message}
        key={vertical + horizontal}
        severity={severity}
        autoHideDuration={5000}
        handleClose={handleCloseSb}
      />
      </div>
  </>


  );
};

EditDetails.prototype = {
  userId: PropTypes.string.isRequired,
  closeModal: PropTypes.func.isRequired
}