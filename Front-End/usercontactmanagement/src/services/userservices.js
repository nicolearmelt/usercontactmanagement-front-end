import axios from 'axios';

axios.defaults.baseURL = 'https://localhost:44395/api/user';

const getData = () => { 
  return axios.get();
};

const getDataById = (id) => {
  return axios.get(`/${id}`);
};

const updateData = (id, data) => {
  return axios.put(`/${id}`, data);
}

const deleteData = (id) => {
  return axios.delete(`/${id}`);
}

export const userServices = {
  getData,
  getDataById,
  updateData,
  deleteData
};
